#include "TextBubble.h"

#include "Node.h"
#include <QPainter>
#include <QGraphicsTextItem>
#include <QStyleOptionGraphicsItem>
#include <QStyle>
#include <QDebug>


TextBubble::TextBubble(Node *node, QString desc)
{
    m_node = node;
    m_desc = desc;
    adjust();
    m_isShow = false;
    m_txtItem = new QGraphicsTextItem(this);

    QFont segoeUI("Segoe UI Regular", 12);
    m_txtItem->setFont(segoeUI);
    m_txtItem->setDefaultTextColor(QColor(208,208,208));
    m_txtItem->setTextWidth(200);

    if(m_desc.trimmed().length() > 320)
    {
        m_desc.truncate(320);
        m_desc += "...";
    }
}

void TextBubble::adjust()
{
    if (!m_node)
        return;

    m_sourcePoint = mapFromItem(m_node, 0, 0);
}

QRectF TextBubble::boundingRect() const
{
    if (!m_node || m_desc.trimmed().isEmpty())
        return QRectF();

    return QRectF(m_sourcePoint, QSizeF(200, 100));
}

void TextBubble::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    if (!m_node)
        return;

    m_txtItem->setHtml(m_desc);

    painter->setPen(QPen(Qt::black, 0));
    painter->setRenderHint(QPainter::Antialiasing);

    if(m_node->getSeqNo()%2 == 0)
    {
        QPointF middle((m_sourcePoint.x() - 70) + (boundingRect().width()/2), (m_sourcePoint.y() + 150) + (boundingRect().height()/2));
        QRadialGradient gradient(middle, 200);
        if (option->state & QStyle::State_Sunken) {
            //gradient.setCenter(3, 3);
            //gradient.setFocalPoint(3, 3);
            gradient.setColorAt(1, QColor(Qt::blue).light(120));
            gradient.setColorAt(0, QColor(Qt::darkBlue).light(120));
        } else {
            //gradient.setColorAt(0, Qt::white);
            gradient.setColorAt(0, QColor(48,70,78));
            gradient.setColorAt(1, QColor(48,70,78));
        }
        painter->setBrush(gradient);

        painter->drawRoundedRect(m_sourcePoint.x() - 70, m_sourcePoint.y() + 50, boundingRect().width(), boundingRect().height() - 10, 10, 10);

        const QPointF points[3] = {
                    QPointF(m_sourcePoint.x() + boundingRect().width()/2, m_sourcePoint.y() + boundingRect().height()),
                    QPointF(m_sourcePoint.x() + boundingRect().width()/2 + 10, m_sourcePoint.y() + boundingRect().height()),
                    QPointF(m_sourcePoint.x(), m_sourcePoint.y()),
                };

        painter->setPen(Qt::NoPen);
        painter->drawConvexPolygon(points, 3);

        m_txtItem->setPos(m_sourcePoint.x() - 70, m_sourcePoint.y() + 50);
    }
    else
    {
        QPointF middle((m_sourcePoint.x() - 70) + (boundingRect().width()/2), (m_sourcePoint.y() - 150) + (boundingRect().height()/2));
        QRadialGradient gradient(middle, 200);
        if (option->state & QStyle::State_Sunken) {
            gradient.setColorAt(1, QColor(Qt::blue).light(120));
            gradient.setColorAt(0, QColor(Qt::darkBlue).light(120));
        } else {
            gradient.setColorAt(0, QColor(48,70,78));
            gradient.setColorAt(1, QColor(48,70,78));
        }
        painter->setBrush(gradient);

        painter->drawRoundedRect(m_sourcePoint.x() - 70, m_sourcePoint.y() - 150, boundingRect().width(), boundingRect().height() - 10, 10, 10);

        painter->setBrush(gradient);
        const QPointF points[3] = {
                    QPointF(m_sourcePoint.x() + boundingRect().width()/2, m_sourcePoint.y() - boundingRect().height()),
                    QPointF(m_sourcePoint.x() + boundingRect().width()/2 + 10, m_sourcePoint.y() - boundingRect().height()),
                    QPointF(m_sourcePoint.x(), m_sourcePoint.y()),
                };
        painter->setPen(Qt::NoPen);
        painter->drawConvexPolygon(points, 3);

        //qDebug() << " *********** m_txtItem " << m_node->getSeqNo() << " - " << m_sourcePoint.x() - 70 << " : " << m_sourcePoint.y() - 150 << endl;

        m_txtItem->setPos(m_sourcePoint.x() - 70, m_sourcePoint.y() - 150);
    }
}

QVariant TextBubble::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    switch (change) {
    case ItemPositionHasChanged:
        qDebug() << "ItemPositionHasChanged" << endl;
        break;
    default:
        break;
    };

    return QGraphicsItem::itemChange(change, value);
}

void TextBubble::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    qDebug() << "mousePressEvent" << endl;
    update();
    QGraphicsItem::mousePressEvent(event);
}

void TextBubble::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    qDebug() << "mouseReleaseEvent" << endl;
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}

bool TextBubble::isShow() const
{
    return m_isShow;
}

void TextBubble::setShow(bool isShow)
{
    m_isShow = isShow;
}


#-------------------------------------------------
#
# Project created by QtCreator 2016-09-12T18:52:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ROWidget
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Edge.cpp \
    Node.cpp \
    TimeLineWidget.cpp \
    TextItem.cpp \
    TextBubble.cpp

HEADERS  += mainwindow.h \
    Edge.h \
    Node.h \
    TimeLineWidget.h \
    TextItem.h \
    TextBubble.h

FORMS    += mainwindow.ui

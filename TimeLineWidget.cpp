#include "TimeLineWidget.h"
#include "Node.h"
#include "Edge.h"

#include <math.h>
#include <QKeyEvent>
#include <QDebug>
#include <QTimer>
#include <QGraphicsTextItem>

TimeLineWidget::TimeLineWidget(QWidget *parent) : QGraphicsView(parent)
{
    m_scene = new QGraphicsScene(this);
    m_scene->setItemIndexMethod(QGraphicsScene::NoIndex);

    m_scene->setSceneRect(0, 0, 500, 600);
    setScene(m_scene);
    setCacheMode(CacheBackground);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    scale(qreal(0.8), qreal(0.8));
    setMinimumSize(400, 400);
    setWindowTitle(tr("Time Line"));

    m_lastNode = nullptr;
    m_ItemNo = 0;

    QGraphicsTextItem *txtHeading = new QGraphicsTextItem();
    QFont segoeUI("Segoe UI Regular", 22);
    txtHeading->setFont(segoeUI);
    txtHeading->setDefaultTextColor(QColor(243,134,48));
    txtHeading->setHtml("Layering Alert (20140729 - 11:25:42)");
    txtHeading->setPos(10, 5);
    m_scene->addItem(txtHeading);

    QGraphicsTextItem *txtHeading2 = new QGraphicsTextItem();
    QFont segoeUI2("Segoe UI Regular", 11);
    txtHeading2->setFont(segoeUI2);
    txtHeading2->setDefaultTextColor(QColor(208,208,208));
    txtHeading2->setHtml("Broker A126756  has placed 3 large buy orders 0.02 ticks below the best bid of instrument FR000013 and has cancelled more than 70% of the layered volume upon receiving execution(s) on the sell side.");
    txtHeading2->setPos(10, 55);
    txtHeading2->setTextWidth(1200);
    m_scene->addItem(txtHeading2);
}

TimeLineWidget::~TimeLineWidget()
{
    foreach (Node *node, m_nodes)
    {
        delete node;
    }
    foreach (Edge* edge, m_edges)
    {
        delete edge;
    }
}

void TimeLineWidget::addNode(QString dateTime, QString desc, QVariant userDate)
{
    m_ItemNo++;
    Node *node = new Node(this, m_ItemNo, dateTime, 1, desc);
    node->setShow(true);
    m_scene->addItem(node);
    m_nodes.push_back(node);
    if(m_lastNode != nullptr)
    {
        addEdge(m_lastNode, node);
    }
    m_lastNode = node;
}

void TimeLineWidget::addEdge(Node* srcnode, Node* destnode/* Qt::GlobalColor edgeColor, const QString& toolTip*/)
{
    Edge* edge = new Edge(srcnode, destnode);
    m_edges.push_back(edge);
    edge->setShow(true);
    m_scene->addItem(edge);
}

void TimeLineWidget::itemMoved()
{

}

void TimeLineWidget::zoomIn()
{
    scaleView(1 / qreal(1.2));
}

void TimeLineWidget::zoomOut()
{
    scaleView(qreal(1.2));
}

void TimeLineWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Up:
        play();
        break;
    case Qt::Key_Down:
        clear();
        break;
    case Qt::Key_Left:

        break;
    case Qt::Key_Right:

        break;
    case Qt::Key_Plus:
        zoomIn();
        break;
    case Qt::Key_Minus:
        zoomOut();
        break;
    case Qt::Key_Space:
    case Qt::Key_Enter:
        //shuffle();
        break;
    default:
        QGraphicsView::keyPressEvent(event);
    }
}

void TimeLineWidget::timerEvent(QTimerEvent *event)
{

}

void TimeLineWidget::wheelEvent(QWheelEvent *event)
{
    scaleView(pow((double)2, -event->delta() / 240.0));
}

void TimeLineWidget::drawBackground(QPainter *painter, const QRectF &rect)
{
    QRectF sceneRect = this->sceneRect();

    painter->fillRect(rect, QColor::fromRgb(31,31,31));
    painter->fillRect(rect.intersected(sceneRect), QColor::fromRgb(31,31,31));
    m_scene->update();
    update();
    updateSceneRect(m_scene->itemsBoundingRect());

    painter->setBrush(Qt::NoBrush);
    sceneRect.adjust(rect.x(),rect.y(),rect.width(),rect.height());
    sceneRect.toAlignedRect();
    painter->drawRect(sceneRect);
}

void TimeLineWidget::scaleView(qreal scaleFactor)
{
    qreal factor = transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if (factor < 0.07 || factor > 100)
        return;

    scale(scaleFactor, scaleFactor);
}

void TimeLineWidget::resizeEvent(QResizeEvent *event)
{
    qDebug() << "TimeLineWidget::resizeEvent : " << endl;
    QGraphicsView::resizeEvent(event);
}

void TimeLineWidget::arrange()
{
    qreal nodeMinimumWidth = 140;
    qreal nodeMinimumHeight = 125;
    qreal screenX = m_scene->sceneRect().width();
    qreal screenY = m_scene->sceneRect().height();
    int nodeCount = m_nodes.size();

    qDebug() << "screenX : " << screenX << ", screenY : " << screenY << ", nodeCount : " << nodeCount << endl;

    qreal nodeX = screenX / (nodeCount + 1);
    qreal nodeY = screenY / 2;

    if(nodeX < nodeMinimumWidth)
    {
        m_scene->setSceneRect(0,0, nodeMinimumWidth * (nodeCount + 1), 600);
        qDebug() << "XXXX screenX : " << screenX << ", screenY : " << screenY << ", nodeCount : " << nodeCount << endl;

         nodeX = m_scene->sceneRect().width() / (nodeCount + 1);
         nodeY = m_scene->sceneRect().height() / 2;

    }

    qreal nodeStartX = nodeX;

    QList<Node*>::iterator itr;
    foreach (Node *node, m_nodes)
    {
        QPointF absPos(nodeStartX, nodeY);
        node->setPos(absPos);
        node->setAbsPos(absPos);
        nodeStartX += nodeX;
    }
}

void TimeLineWidget::clear()
{
    foreach (Node *node, m_nodes)
    {
        node->setShow(false);
        scene()->removeItem(node);
    }
    foreach (Edge* edge, m_edges)
    {
        edge->setShow(false);
        scene()->removeItem(edge);
    }
}

void TimeLineWidget::play()
{
    m_playTimer = new QTimer(this);
    connect(m_playTimer, SIGNAL(timeout()), this, SLOT(onPlay()));
    m_playTimer->start(2000);
}

void TimeLineWidget::pause()
{

}

void TimeLineWidget::stop()
{

}

void TimeLineWidget::onPlay()
{
    foreach (Node *node, m_nodes)
    {
        if(!node->isShow())
        {
            node->setShow(true);
            m_scene->addItem(node);

            if(node->getSeqNo() != 1)
            {
                foreach (Edge* edge, m_edges)
                {
                    if(!edge->isShow())
                    {
                        edge->setShow(true);
                        m_scene->addItem(edge);
                        return;
                    }
                }
            }
            return;
        }
    }
}

void TimeLineWidget::onPause()
{

}

void TimeLineWidget::onStop()
{

}

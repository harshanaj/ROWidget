#include "TextItem.h"

#include <QTextBlockFormat>
#include <QTextCursor>
#include <QTextDocument>

TextItem::TextItem(QGraphicsItem* parent)
    : QGraphicsTextItem(parent),
      alignment_(Qt::AlignCenter)
{
    init();
}

TextItem::TextItem(const QString& text, QGraphicsItem* parent)
    : QGraphicsTextItem(text, parent),
      alignment_(Qt::AlignCenter)
{
    init();
}

void TextItem::init()
{
    updateGeometry();
    QTextDocument* textDoc = document();
    connect(textDoc, SIGNAL(contentsChange(int, int, int)),
            this, SLOT(updateGeometry(int, int, int)));
}

void TextItem::setAlignment(Qt::Alignment alignment)
{
    alignment_ = alignment;
    QTextBlockFormat format;
    format.setAlignment(alignment);
    QTextCursor cursor = textCursor();      // save cursor position
    int position = textCursor().position();
    cursor.select(QTextCursor::Document);
    cursor.mergeBlockFormat(format);
    cursor.clearSelection();
    cursor.setPosition(position);           // restore cursor position
    setTextCursor(cursor);
}

void TextItem::updateGeometry(int, int, int)
{
    updateGeometry();
}

void TextItem::updateGeometry()
{
    QPointF topRightPrev = boundingRect().topRight();
    setTextWidth(-1);
    setTextWidth(boundingRect().width());
    setAlignment(alignment_);
    QPointF topRight = boundingRect().topRight();

    if (alignment_ & Qt::AlignRight)
    {
        setPos(pos() + (topRightPrev - topRight));
    }
}

int TextItem::type() const
{
    return Type;
}



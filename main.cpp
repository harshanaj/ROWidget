#include "mainwindow.h"
#include <QApplication>
#include "TimeLineWidget.h"
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    TimeLineWidget *timeLineWidget = new TimeLineWidget;
    timeLineWidget->addNode("20160729-11:21:06", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has placed a large <span style=\"color: #66a5e2;\">buy</span> order at <span style=\"color: #66a5e2;\">0.73, 0.01</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:22:13", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has placed a large <span style=\"color: #66a5e2;\">buy</span> order at <span style=\"color: #66a5e2;\">0.73, 0.01</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:22:56", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has placed a large <span style=\"color: #66a5e2;\">buy</span> order at <span style=\"color: #66a5e2;\">0.73, 0.02</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:23:11", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has placed a large <span style=\"color: #66a5e2;\">buy</span> order at<span style=\"color: #66a5e2;\"> 0.73, 0.03</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:24:55", "<p>Broker <span style=\"color: #e53243;\">A126756</span> has received an execution on the <span style=\"color: #e53243;\">sell</span> side for <span style=\"color: #e53243;\">4000</span> shares at <span style=\"color: #e53243;\">0.76 </span></p>","");
    timeLineWidget->addNode("20160729-11:25:06", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has cancelled a large <span style=\"color: #66a5e2;\">buy</span> order at <span style=\"color: #66a5e2;\">0.73. 0.01</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:25:34", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has cancelled a large <span style=\"color: #66a5e2;\">buy</span> order at<span style=\"color: #66a5e2;\"> 0.73. 0.01</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:25:42", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has cancelled a large <span style=\"color: #66a5e2;\">buy</span> order at <span style=\"color: #66a5e2;\">0.73. 0.01</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:24:55", "<p>Broker <span style=\"color: #e53243;\">A126756</span> has received an execution on the <span style=\"color: #e53243;\">sell</span> side for <span style=\"color: #e53243;\">4000</span> shares at <span style=\"color: #e53243;\">0.76 </span></p>","");
    timeLineWidget->addNode("20160729-11:25:06", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has cancelled a large <span style=\"color: #66a5e2;\">buy</span> order at <span style=\"color: #66a5e2;\">0.73. 0.01</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:25:34", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has cancelled a large <span style=\"color: #66a5e2;\">buy</span> order at<span style=\"color: #66a5e2;\"> 0.73. 0.01</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:25:42", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has cancelled a large <span style=\"color: #66a5e2;\">buy</span> order at <span style=\"color: #66a5e2;\">0.73. 0.01</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:25:06", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has cancelled a large <span style=\"color: #66a5e2;\">buy</span> order at <span style=\"color: #66a5e2;\">0.73. 0.01</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:25:34", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has cancelled a large <span style=\"color: #66a5e2;\">buy</span> order at<span style=\"color: #66a5e2;\"> 0.73. 0.01</span> points below the best bid</p>","");
    timeLineWidget->addNode("20160729-11:25:42", "<p>Broker <span style=\"color: #66a5e2;\">A126756</span> has cancelled a large <span style=\"color: #66a5e2;\">buy</span> order at <span style=\"color: #66a5e2;\">0.73. 0.01</span> points below the best bid</p>","");

    /*QString text = "&lt;p&gt;&lt;span style=&quot;color: #66a5e2;&quot;&gt;&lt;strong&gt;Firm A&lt;/strong&gt;&lt;/span&gt; has submitted a large buy side order firAb123 of visible order size &lt;strong&gt;&lt;span style= 7N`c&gt;5,000&lt;/span&gt;&lt;/strong&gt; at a price of&lt;/span&gt;&lt;strong&gt;&lt;span style=&quot;color: #66a5e2;&quot;&gt; 21.99.&lt;";
    text = text.replace("&gt;",">").replace("&lt;","<").replace("&quot;","\"");
    qDebug() << text << endl;
    timeLineWidget->addNode("20160605-12:55:32.021", text+text+text,"");
    //timeLineWidget->addNode("20160605-12:55:32.021","","");
    timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #66a5e2;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #66a5e2;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #66a5e2;\">21.99.</span></strong></p>","");
    timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #ff9900;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #ff9900;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #ff9900;\">21.99.</span></strong></p>","");
    timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #66a5e2;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #66a5e2;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #66a5e2;\">21.99.</span></strong></p>","");
    timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #ff9900;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #ff9900;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #ff9900;\">21.99.</span></strong></p>","");
    timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #66a5e2;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #66a5e2;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #66a5e2;\">21.99.</span></strong></p>","");
    timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #ff9900;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #ff9900;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #ff9900;\">21.99.</span></strong></p>","");
*/
/*
    timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #ff9900;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #ff9900;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #ff9900;\">21.99.</span></strong></p>","");
    timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #66a5e2;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #66a5e2;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #66a5e2;\">21.99.</span></strong></p>","");
    timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #ff9900;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #ff9900;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #ff9900;\">21.99.</span></strong></p>","");
    timeLineWidget->addNode("20160605-12:55:32.021",
                           "<p><span style=\"color: #66a5e2;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #66a5e2;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #66a5e2;\">21.99.</span></strong></p>","");
   timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #ff9900;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #ff9900;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #ff9900;\">21.99.</span></strong></p>","");
    timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #66a5e2;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #66a5e2;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #66a5e2;\">21.99.</span></strong></p>","");
    timeLineWidget->addNode("20160605-12:55:32.021",
                            "<p><span style=\"color: #ff9900;\"><strong>Firm A</strong></span><span>&nbsp;&nbsp;has submitted a large buy side order firAb123 of visible order size</span><strong>&nbsp;<span style=\"color: #ff9900;\">5,000</span></strong><span>&nbsp;at a price of</span><strong>&nbsp;<span style=\"color: #ff9900;\">21.99.</span></strong></p>","");
*/
    QMainWindow mainWindow;
    mainWindow.setCentralWidget(timeLineWidget);
    mainWindow.setWindowTitle("Reasoning Out Timeline - ALERT_2016072911245980");

    timeLineWidget->arrange();

    mainWindow.show();

    return app.exec();
}

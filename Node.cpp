#include "Edge.h"
#include "Node.h"
#include "TimeLineWidget.h"
#include "TextItem.h"
#include "TextBubble.h"

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOption>
#include <QDebug>
#include <QFontMetrics>
#include <QTextEdit>
#include <QGraphicsProxyWidget>

Node::Node(TimeLineWidget *timeLineWidget, int seqNo,  QString dateTime, int itemType, QString desc)
    : m_timeLineWidget(timeLineWidget)
{
    setFlag(ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setZValue(-1);

    m_seqNo = seqNo;
    m_isShow = false;

    QString format = "yyyyMMdd-hh:mm:ss.zzz";
    m_dateTime = QDateTime::fromString(dateTime, format);

    m_timeText = new QGraphicsTextItem(dateTime, this);
    QFont segoeUI("Segoe UI Regular", 12);
    m_timeText->setFont(segoeUI);
    m_timeText->setDefaultTextColor(QColor(159,194,205));

    m_textBubble = new TextBubble(this, desc);
    m_timeLineWidget->scene()->addItem(m_textBubble);
}

Node::~Node()
{

}

void Node::addEdge(Edge *edge)
{
    edgeList << edge;
    edge->adjust();
}

QList<Edge *> Node::edges() const
{
    return edgeList;
}

bool Node::advance()
{
    if (newPos == pos())
        return false;

    setPos(newPos);
    return true;
}

QRectF Node::boundingRect() const
{
    qreal adjust = 10;
    return QRectF( -10 - adjust, -10 - adjust, 23 + adjust, 23 + adjust);
}

QPainterPath Node::shape() const
{
    QPainterPath path;
    path.addEllipse(-20, -20, 30, 30);
    return path;
}

void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    QPen textPen();
    QPen linePen();
    painter->setRenderHint(QPainter::Antialiasing);
    painter->setRenderHint(QPainter::HighQualityAntialiasing);

    QRadialGradient gradient(-3, -3, 10);
    if (option->state & QStyle::State_Sunken) {
        gradient.setCenter(3, 3);
        gradient.setFocalPoint(3, 3);
        gradient.setColorAt(1, QColor(Qt::blue).light(120));
        gradient.setColorAt(0, QColor(Qt::darkBlue).light(120));
    } else {
        gradient.setColorAt(0, QColor(93,128,208));
        gradient.setColorAt(1, QColor(250,180,60));
    }

    if(m_seqNo == 5)
    {
        gradient.setColorAt(0, QColor(130,100,180));
        gradient.setColorAt(1, QColor(0,152,89));
    }

    if(m_seqNo == 6 || m_seqNo == 7 || m_seqNo == 8)
    {
        gradient.setColorAt(0, QColor(205,51,66));
        gradient.setColorAt(1, QColor(205,51,66));
    }

    painter->setBrush(gradient);

    painter->setPen(QPen(Qt::black, 0));
    painter->drawEllipse(-20, -20, 30, 30);

    QRectF textRect = boundingRect();
    textRect.setX(textRect.x()+10);
    textRect.setY(textRect.y()+8);
    painter->setPen(QPen(Qt::white, 0));
    painter->drawText(textRect, QString::number(m_seqNo));

    QPointF nodePoint = mapToScene(textRect.x(), textRect.y());
    QPointF toolTipPoint;
    if(m_seqNo % 2 == 0)
    {
        m_timeText->setPos(textRect.x() - 80, textRect.y() - 40);
    }
    else
    {
        m_timeText->setPos(textRect.x() - 80, textRect.y() + 30);
    }
}

qint64 Node::getTimeMSecsSinceEpoch()
{
    return m_dateTime.toMSecsSinceEpoch();
}

QVariant Node::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch (change) {
    case ItemPositionHasChanged:
        foreach (Edge *edge, edgeList)
        {
            edge->adjust();
        }
        m_textBubble->adjust();
        m_timeLineWidget->itemMoved();
        break;
    default:
        break;
    };

    return QGraphicsItem::itemChange(change, value);
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    qDebug() << "Node::mousePressEvent" << endl;
    update();
    QGraphicsItem::mousePressEvent(event);
}

void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}

int Node::getSeqNo() const
{
    return m_seqNo;
}

bool Node::isShow() const
{
    return m_isShow;
}

void Node::setShow(bool isShow)
{
    m_isShow = isShow;
}

void Node::setAbsPos(const QPointF& absPos)
{
    m_absPos = absPos;
}

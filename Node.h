#ifndef NODE_H
#define NODE_H

#include <QGraphicsItem>
#include <QList>
#include <QDateTime>

class Edge;
class TimeLineWidget;
class TextItem;
class QTextEdit;
class TextBubble;

QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE

class Node: public QGraphicsItem
{
public:
    Node(TimeLineWidget *timeLineWidget, int seqNo, QString dateTime, int itemType, QString desc);
    virtual ~Node();
    void addEdge(Edge *edge);
    QList<Edge *> edges() const;

    enum { Type = UserType + 1 };
    int type() const Q_DECL_OVERRIDE { return Type; }

    bool advance();

    QRectF boundingRect() const Q_DECL_OVERRIDE;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;

    qint64 getTimeMSecsSinceEpoch();

    void setAbsPos(const QPointF& absPos);

    bool isShow() const;
    void setShow(bool isShow);

    int getSeqNo() const;

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;

    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;

private:
    int m_seqNo;
    bool m_isShow;
    QPointF newPos;
    QPointF m_absPos;
    QList<Edge*> edgeList;

    qreal m_descScaleFact;

    QDateTime m_dateTime;
    QTextEdit* m_txtEdit;
    QGraphicsTextItem* m_desc;
    QGraphicsTextItem* m_timeText;

    TimeLineWidget *m_timeLineWidget;
    TextBubble* m_textBubble;
};

#endif // NODE_H

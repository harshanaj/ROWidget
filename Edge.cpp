#include "Edge.h"
#include "Node.h"

#include <math.h>
#include <QPainter>

Edge::Edge(Node *sourceNode, Node *destNode)
{
    setAcceptedMouseButtons(0);
    m_source = sourceNode;
    m_dest = destNode;
    m_source->addEdge(this);
    m_dest->addEdge(this);
    adjust();

    m_isShow = false;
}

Node *Edge::sourceNode() const
{
    return m_source;
}

Node *Edge::destNode() const
{
    return m_dest;
}

void Edge::adjust()
{
    if (!m_source || !m_dest)
        return;

    QLineF line(mapFromItem(m_source, 0, 0), mapFromItem(m_dest, 0, 0));
    qreal length = line.length();

    prepareGeometryChange();

    if (length > qreal(20.)) {
        QPointF edgeOffset((line.dx() * 10) / length, (line.dy() * 10) / length);
        m_sourcePoint = line.p1() + edgeOffset;
        m_destPoint = line.p2() - edgeOffset;
    } else {
        m_sourcePoint = m_destPoint = line.p1();
    }
}

QRectF Edge::boundingRect() const
{
    if (!m_source || !m_dest)
        return QRectF();

    return QRectF(m_sourcePoint, QSizeF(m_destPoint.x() - m_sourcePoint.x(),
                                      m_destPoint.y() - m_sourcePoint.y()))
        .normalized();
}

void Edge::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    if (!m_source || !m_dest)
        return;

    QLineF line(m_sourcePoint, m_destPoint);
    if (qFuzzyCompare(line.length(), qreal(0.)))
        return;

    // Draw the line itself
    painter->setPen(QPen(QColor(86,116,126), 2, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin));
    painter->drawLine(line);

    // Draw the arrows
    //double angle = ::acos(line.dx() / line.length());
    //if (line.dy() >= 0)
    //    angle = TwoPi - angle;

    painter->setBrush(Qt::black);
}

bool Edge::isShow() const
{
    return m_isShow;
}

void Edge::setShow(bool isShow)
{
    m_isShow = isShow;
}

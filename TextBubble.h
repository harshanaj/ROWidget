#ifndef TEXTBUBBLE_H
#define TEXTBUBBLE_H

#include <QGraphicsItem>

class QGraphicsTextItem;
class Node;

QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE

class TextBubble : public QGraphicsItem
{
public:
    TextBubble(Node *node, QString desc = "");

    bool isShow() const;
    void setShow(bool isShow);

    void adjust();

    enum { Type = UserType + 2 };
    int type() const Q_DECL_OVERRIDE { return Type; }

protected:
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;

    QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;

    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;

private:
    Node *m_node;
    QPointF m_sourcePoint;
    bool m_isShow;
    QString m_desc;
    QGraphicsTextItem* m_txtItem;
};

#endif // TEXTBUBBLE_H

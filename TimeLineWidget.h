#ifndef TIMELINEWIDGET_H
#define TIMELINEWIDGET_H

#include <QGraphicsView>

class Node;
class Edge;
class QTimer;

class TimeLineWidget : public QGraphicsView
{
        Q_OBJECT
public:
    TimeLineWidget(QWidget *parent = 0);
    virtual ~TimeLineWidget();
    void addNode(QString time, QString desc, QVariant userDate);
    void addEdge(Node* srcnode, Node* destnode);
    void itemMoved();
    void arrange();
    void clear();

public slots:
    void zoomIn();
    void zoomOut();
    void play();
    void pause();
    void stop();

protected:
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void timerEvent(QTimerEvent *event) Q_DECL_OVERRIDE;
#ifndef QT_NO_WHEELEVENT
    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
#endif
    void drawBackground(QPainter *painter, const QRectF &rect) Q_DECL_OVERRIDE;
    void scaleView(qreal scaleFactor);
    void resizeEvent (QResizeEvent * event);

private slots:
    void onPlay();
    void onPause();
    void onStop();

private:
    QList<Node*> m_nodes;
    QList<Edge*> m_edges;

    QGraphicsScene *m_scene;
    Node *m_lastNode;
    int m_ItemNo;

    QTimer *m_playTimer;
};

#endif // TIMELINEWIDGET_H
